package tennis

class TennisGame3(val playerOneName: String, val playerTwoName: String)
    extends TennisGame {
  private val playerOne = new Player(playerOneName)
  private val playerTwo = new Player(playerTwoName)

  private val separator = "-"

  def calculateScore(): String = {
    if (notFinalStage) {
      if (playerOne.points == playerTwo.points) playerOne.score + separator + "All"
      else playerOne.score + separator + playerTwo.score
    } else {
      if (playerOne.points == playerTwo.points) "Deuce"
      else showLeader
    }
  }

  def wonPoint(playerName: String): Unit = {
    if (playerName == playerOneName)
      playerOne.wonPoint()
    else if (playerName == playerTwoName)
      playerTwo.wonPoint()
    else ()
  }

  def notFinalStage: Boolean = playerOne.points < 4 && playerTwo.points < 4 && playerOne.points + playerTwo.points < 6

  def showLeader: String = {
    val leader =
      if (playerOne.points > playerTwo.points) playerOneName
      else playerTwoName
    val scoreDiff = Math.abs(playerOne.points - playerTwo.points)
    if (scoreDiff == 1)
      "Advantage " + leader
    else "Win for " + leader
  }
}

private class Player(val name: String) {
  private var pointsCounter = 0

  def score: String = points match {
    case 0 => "Love"
    case 1 => "Fifteen"
    case 2 => "Thirty"
    case 3 => "Forty"
    case _ => "Advantage/Win"
  }

  def points: Int = pointsCounter

  def wonPoint(): Unit = {
    pointsCounter += 1
  }
}
